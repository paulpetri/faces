angular.module('pmp.UserService', []).factory('UserService', [ '$http',

function($http) {

	var UserService = {};

	UserService.logInUser = function(username, password, token) {

		return $http({
			url : '/Faces/services/user/login',
			method : "POST",
			dataType : 'application/x-www-form-urlencoded',
			headers : {
				'token' : token,
				'username' : username,
				'password' : password
			},
		});
	};

	UserService.createAccount = function(username, password, profilePhoto) {

		console.log('the Image array:::   ' + profilePhoto);

		return $http({
			url : '/Faces/services/user/signin',
			method : "POST",
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			data : {

				'username' : username,
				'password' : password,
				'profilePhoto' : profilePhoto
			}

		});
	}

	UserService.googleLogin = function(gToken) {

		return $http({
			url : '/Faces/services/user/google',
			method : "POST",

			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			data : {
				'gToken' : gToken
			}

		});
	}

	UserService.getUserByUsername = function(username) {

		return $http({
			url : "/Faces/rest/users/geUserByUsername",
			method : "POST",
			headers : {
				'username' : username,
			}
		});

	}

	UserService.getUsers = function(token) {

		return $http({
			url : '/Faces/services/user/all',
			method : 'GET',
			headers : {
				'token' : token 
			}
		});
	}

	return UserService;

} ]);
