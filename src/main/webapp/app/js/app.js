'use strict';

window.angular.module('services', ['pmp.UserService']);

window.angular.module('controllers', ['pmp.logInController',
                                      'pmp.photoUploadController',
                                      'pmp.googleController',
                                      'pmp.userWindowController',
                                      'pmp.adminWindowController'
                                      ]);

window.angular.module('directives', ['pmp.uploadDirective']);

window.facesApp = angular.module('facesApp', [ 			
                                              			'ngResource',
                                                 		'ngRoute',
                                                 		'services',
                                                 		'controllers',
                                                 		'directives',
                                                 	
                                                 		
                                                 		
                                                 		]).config( function($routeProvider, $locationProvider) {
                                                 			
                                                 					$routeProvider
                                                 								
                                                 								.when("/home", 
                                                 									{templateUrl : "app/views/home.html"})
                                                 								
                                                 								.when("/logIn", 
                                                 									 {templateUrl : "app/views/newLogIn.html"})
                                                 									 
                                                 								.when("/mainWindow", 
                                                 									 {templateUrl : "app/views/mainWindow.html"
                                                 									 })
                                                 								
                                                 								.when("/createAccount", 
                                                    									 {templateUrl : "app/views/createAccount.html",
                                                    									  controller: 'photoUploadController'	 
                                                    									 })
                                                    							.when("/userWindow",
                                                    									{templateUrl : "app/views/userWindow.html",
                                                    									 controller: "userWindowController"
                                                    									})
                                                    							
                                                    							.when("/adminWindow",
                                                    									{templateUrl : "app/views/adminView.html",
                                                    									 controller: "adminWindowController"
                                                    									})
                                                    									 
                                                 								.otherwise({
				                                                 	                redirectTo: '/logIn'
				                                                 	            });  
                                                 		});