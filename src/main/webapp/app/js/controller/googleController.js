angular.module('pmp.googleController', [])
	.controller('googleController', [
			'$scope',
			'$location',
			'UserService',
			
			
			function($scope, $location, UserService) {
                                       
					$scope.getToken = function(){
						
						var client_id ='706960869932-6cfmcbkugo3qb9f8f1c5a36u1f264tal.apps.googleusercontent.com';
						var scope ='email';
						var redirect_uri='http://localhost:8080/Faces/services/user/google';
						var response_type ='token';
						
						window.gapi.auth.authorize({
							    client_id:'706960869932-6cfmcbkugo3qb9f8f1c5a36u1f264tal.apps.googleusercontent.com',
							    scope: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email'
							    }).then(function(data) {
							        var accessToken=data.access_token;
							        
							       
							        
							        UserService.googleLogin(accessToken).success(function(user) {
							        	
							        	window.localStorage.setItem("googleEmail", user.email);
							        	window.localStorage.setItem("googleProfilePhoto", user.picture);
							        	console.log(user.email);
							        	console.log(user.picture);
							        	
							        	$location.path("/userWindow");
							        	$scope.googleProfilePhoto =user.picture;
							        	
							        });
					
							    });
					
                         
				}
                            			
                            			
           }]);