
angular.module('pmp.photoUploadController', [])
	.controller('photoUploadController', [
			'UserService',
			'$scope',
			'$location',
			'fileReader',
			function(UserService, $scope, $location, fileReader) {
					
					$scope.singIn = function(){
						console.log("button pressed");
					}
				
					 $scope.getFile = function () {
						 
					        fileReader.readAsDataUrl($scope.file, $scope)
					                      .then(function(result) {
					                          $scope.imageSrc = result;
					                          console.log($scope.file);
					                          console.log(result);
					                      });
					 };
					 
					$scope.createAccount = function(){
						 
						console.log($scope.txtUsername+ " " +$scope.txtPassword +" "+ $scope.imageSrc );
						
						var reader = new FileReader();
						console.log(reader);
						
						UserService.createAccount($scope.txtUsername, $scope.txtPassword, $scope.imageSrc).success(function(response) {
                        	 if (response.error == 0) {
                        		 
            					/*$location.path("/mainWindow");*/
            				}
            			});
					 
					 }
					
					$scope.getUserByUsername = function(){
												
						UserService.getUserByUsername($scope.txtUsername).success(function(response) {
                        	 console.log(response);
                        	 if (response.error == 0) {
                        		
                        		 console.log(response.username);
                        		 console.log(response.photo);
                        		
                        		 $scope.profilePhoto =response.photo.substring(1, response.photo.length-4);
            				
                        	 
                        	 }
            			});
					 
					 }
			
                            			
                            			
           }]);
