angular.module('pmp.userWindowController', [])
	.controller('userWindowController', [
			'UserService',
			'$scope',
			'$location',
			function(UserService, $scope, $location) {
				
				$scope.googleProfilePhoto = window.localStorage.getItem("googleProfilePhoto");
				$scope.googleEmail = window.localStorage.getItem("googleEmail");
				$scope.IsVisible= false;
				
				$scope.ShowHide = function () {
					$scope.IsVisible = $scope.IsVisible ? false : true;
				}
				
				$scope.logOut = function () {
					$location.path("/logIn");
				}
           }]);
