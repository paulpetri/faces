/*
 *This is the login controller 
 * 
 */

angular.module('pmp.logInController', [])
	.controller('logInController', [
			'UserService',
			'$scope',
			'$location',
			
			function(UserService, $scope, $location) {
                                       
					$scope.singIn = function(){
                            				
                         UserService.logInUser($scope.txtUsername, $scope.txtPassword)
                         	.success(function(response) {
                         	
                         	if(response){
                         		
                         		//the token is set in local storage for further use in each request
                         		window.localStorage.setItem("token", response.user.token);
                         		console.log('Try to get the roles ---> ');
                         		console.log(response.user.roles);
                        		console.log(response.user.roles[0].roleName);
                         		
                         		if(response.user.roles[0].roleName == 'ADMINISTRATOR'){
                         			$location.path("/adminWindow");
                         		}
                         		
                         		if(response.user.roles[0].roleName == 'USER'){
                         			$location.path("/userWindow");
                         		}
                         		
                         	}
                         	
                        	if (user.error == 0) {
                        		
            					//$location.path("/userWindow");
            					
            				}
            			});
                         
					}
                            			
                            			
           }]);
