<!DOCTYPE HTML>
<html data-ng-app="facesApp">
<head>
	<title> Angular Test </title>
	
	<!------------- css files ---------->
	<link href="style/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="style/css/clndr.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="style/css/fd-slider.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="style/css/graph.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="style/css/jplayer.blue.monday.min.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="style/css/jquery.nouislider.css" rel="stylesheet" type="text/css" media="all"/>
	<link href="style/css/style.css" rel="stylesheet" type="text/css" media="all"/>
	
<!-- 	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 -->
	<!--------------Js--------------->

	<script src="js/jquery-1.11.1.min.js"></script> 
	<script src="js/modernizr.custom.js"></script>
	
	<!-- <script src="style/js/bootstrap.js"></script>
	<script src="style/js/Chart.js"></script>
	<script src="style/js/clndr.js"></script>
	<script src="style/js/easing.js"></script>
	<script src="style/js/fd-slider.js"></script>
	<script src="style/js/jquery.circlechart.js"></script>
	<script src="style/js/jquery.flot.min.js"></script>
	<script src="style/js/jquery.jplayer.min.js"></script>
	<script src="style/js/jquery.nouislider.js"></script>
  	<script src="style/js/jquery-1.11.1.min.js"></script> 
  	<script src="style/js/modernizr.custom.js"></script>
  	<script src="style/js/moment-2.2.1.js"></script>
  	<script src="style/js/move-top.js"></script>
  	<script src="style/js/site.js"></script>
  	<script src="style/js/underscore-min.js"></script> -->
	
	<!-- Angular libraries -->
	<script src="angular-resouce/angular.min.js"></script>
	<script src="angular-resouce/angular-resource.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
	<script src="https://apis.google.com/js/client.js"></script>
	
	<!-- Angular Controllers -->
	<script src="app/js/controller/logInController.js"></script>
	<script src="app/js/controller/photoUploadController.js"></script>
	<script src="app/js/controller/googleController.js"></script>
	<script src="app/js/controller/userWindowController.js"></script>
	<script src="app/js/controller/adminWindowController.js"></script>
	
	<!-- Angular Service -->
	<script src="app/js/services/UserService.js"></script>
	
	<!-- Angular Service -->
	<script src="app/js/directives/uploadDirective.js"></script>
	
	<!-- Application JS -->
	<script src="app/js/app.js"></script>
	<script src="app/js/uploadFileReader.js"></script>
	
	
</head>


<body>
	
	<h2>Angular Test!</h2>
	
	<div ng-view > </div>
  
</body>


</html>
