package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;

import net.minidev.json.JSONObject;

/**
 * @author PPaul
 *
 */
public class RequestFilter implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext request) throws IOException {

		JWTTokenUtil tokenUtil = new JWTTokenUtil();
		String requestPath = request.getUriInfo().getPath().toString();
		String token = request.getHeaderString("token");
		
		switch(requestPath){
			case "user/all" : 
				JSONObject user = tokenUtil.getUserJSONFromJWT(token);
				break;
		};
		
		
	}

	public void getRequestBody(ContainerRequestContext request) {
		System.out.println("I am in get request body");
		InputStream io = request.getEntityStream();

		BufferedReader reader = new BufferedReader(new InputStreamReader(io));
		StringBuilder out = new StringBuilder();
		String line;

		try {
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(out);

	}

}
