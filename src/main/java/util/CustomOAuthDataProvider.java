package util;

import java.util.List;

import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.rs.security.oauth2.common.AccessTokenRegistration;
import org.apache.cxf.rs.security.oauth2.common.Client;
import org.apache.cxf.rs.security.oauth2.common.OAuthPermission;
import org.apache.cxf.rs.security.oauth2.common.ServerAccessToken;
import org.apache.cxf.rs.security.oauth2.common.UserSubject;
import org.apache.cxf.rs.security.oauth2.grants.code.AuthorizationCodeDataProvider;
import org.apache.cxf.rs.security.oauth2.grants.code.AuthorizationCodeRegistration;
import org.apache.cxf.rs.security.oauth2.grants.code.ServerAuthorizationCodeGrant;
import org.apache.cxf.rs.security.oauth2.provider.OAuthServiceException;
import org.apache.cxf.rs.security.oauth2.tokens.bearer.BearerAccessToken;

public class CustomOAuthDataProvider implements  AuthorizationCodeDataProvider   {
	
	public Client createClient (){
		
		Client client = new Client(null, null, false);
		
		return null;
	}
	
	public AccessTokenRegistration getAccessTokenRegistration(){
		WebClient wc;
	
		AccessTokenRegistration at = new AccessTokenRegistration();
		
		at.setClient(createClient());
		
		return null;
		
	}
	
	@Override
	public List<OAuthPermission> convertScopeToPermissions(Client arg0,
			List<String> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServerAccessToken createAccessToken(AccessTokenRegistration arg0)
			throws OAuthServiceException {
		
		ServerAccessToken token = new BearerAccessToken(arg0.getClient(), 3600L);
		
		return null;
	}

	@Override
	public ServerAccessToken getAccessToken(String arg0)
			throws OAuthServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Client getClient(String arg0) throws OAuthServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServerAccessToken getPreauthorizedToken(Client arg0,
			List<String> arg1, UserSubject arg2, String arg3)
			throws OAuthServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServerAccessToken refreshAccessToken(Client arg0, String arg1,
			List<String> arg2) throws OAuthServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAccessToken(ServerAccessToken arg0)
			throws OAuthServiceException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ServerAuthorizationCodeGrant createCodeGrant(
			AuthorizationCodeRegistration arg0) throws OAuthServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServerAuthorizationCodeGrant removeCodeGrant(String arg0)
			throws OAuthServiceException {
		// TODO Auto-generated method stub
		return null;
	}

}
