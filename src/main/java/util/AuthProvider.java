package util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author PPaul
 *
 */

public class AuthProvider {


  /**
   * Used to provide a hashed password using Spring BCrypt
   * 
   * @param password
   * @return hashed password
   */

  public String hashPassword(String password) {

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    String hashedPassword = encoder.encode(password);

    return hashedPassword;
  }


  /**
   * @param password
   * @param encodedPassword
   * @return match if password matches or not
   */
  public boolean checkPassword(String password, String encodedPassword) {

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    boolean match = encoder.matches(password, encodedPassword);
    return match;
  }

  public void generateToken() {

  }

  public boolean checkTokenAvailability() {
    return false;
  }


}
