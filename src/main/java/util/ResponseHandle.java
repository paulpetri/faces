package util;

import javax.ws.rs.core.Response;


import com.pmp.exceptions.RequestException;

public class ResponseHandle {
	
	
	public Response buildResponse(RequestException e){
		
		Response response= null;
		
		switch (e.getCause().getMessage()) {
          	
		  	case RequestException.BAD_REQUEST:
		  		response = Response.status(Response.Status.BAD_REQUEST).entity("{\"message\":\""+e.getMessage()+"\"}").build();
                   break;
                   
		  	case RequestException.METHOD_NOT_ALLOWED:
		  		response = Response.status(Response.Status.METHOD_NOT_ALLOWED).entity("{\"message\":\""+e.getMessage()+"\"}").build();
                   break;
                   
		  	case RequestException.UNAUTORIZED:
		  		response = Response.status(Response.Status.UNAUTHORIZED).entity("{\"message\":\""+e.getMessage()+"\"}").build();
                   break;
                   
			case RequestException.NOT_FOUND:
		  		response = Response.status(Response.Status.NOT_FOUND).entity("{\"message\":\""+e.getMessage()+"\"}").build();
                   break;
		  }
		  

		return response;
	}
}
