package com.pmp.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.pmp.dao.UserDAO;
import com.pmp.model.Role;
import com.pmp.model.User;



/**
 * @author PPaul
 *
 */
public class UserDAOImpl implements UserDAO {

  @Autowired
  private SessionFactory sessionFactory;

  /**
   * @return sessionFactory
   */
  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  /**
   * @param sessionFactory
   */


  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Long createAccount(User user) {
	  
	  try {
		 
		  return (Long) sessionFactory.getCurrentSession().save(user);
	  
	  } catch (Exception e) {
	      e.printStackTrace();
	  }
	
	 return null;

  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public User findByUsername(String username) {

    User user = null;
    try {
      Query query =
          sessionFactory.getCurrentSession()
              .createSQLQuery("SELECT * FROM User u where u.username = :username ")
              .addEntity(User.class).setParameter("username", username);

      user = (User) query.uniqueResult();

    } catch (Exception e) {
      e.printStackTrace();
    }

    return user;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public List<User> getAllUsers() {
    List<User> allUsers = null;
    try {
      Query query =
          sessionFactory.getCurrentSession().createSQLQuery("SELECT * FROM User")
              .addEntity(User.class);

      allUsers = query.list();

    } catch (Exception e) {
      e.printStackTrace();
    }
    return allUsers;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public Role getRoleByName(String roleName) {
	  Role role = null;
	    try {
	      Query query =
	          sessionFactory.getCurrentSession()
	              .createSQLQuery("SELECT * FROM Role r where r.roleName = :roleName ")
	              .addEntity(Role.class).setParameter("roleName", roleName);

	      role = (Role) query.uniqueResult();

	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    
	    return role;
  }

@Override
@Transactional
public User findByEmail(String email) {
	
	User user = null;
    try {
      Query query =
          sessionFactory.getCurrentSession()
              .createSQLQuery("SELECT * FROM User u where u.email = :email ")
              .addEntity(User.class).setParameter("email", email);

      user = (User) query.uniqueResult();

    } catch (Exception e) {
      e.printStackTrace();
    }

    return user;
}

@Override
@Transactional
public void setUserActive(boolean userActive, User user) {
	 user.setIsActive(userActive);
	 sessionFactory.getCurrentSession().update(user);
	
}

@Override
@Transactional
public User findByToken(String token) {
	
	User user = null;
    try {
      Query query =
          sessionFactory.getCurrentSession()
              .createSQLQuery("SELECT * FROM User u where u.token = :token ")
              .addEntity(User.class).setParameter("token", token);

      user = (User) query.uniqueResult();

    } catch (Exception e) {
      e.printStackTrace();
    }

    return user;
	
}
}
