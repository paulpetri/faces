package com.pmp.dao;

import java.util.List;

import com.pmp.model.Role;
import com.pmp.model.User;

/**
 * @author PPaul
 *
 */
public interface UserDAO {

  /**
   * @param User
   * @return userId
   */
  public Long createAccount(User user);

  /**
   * @param username
   * @return User found with this username
   */
  public User findByUsername(String username);

  /**
   * @param email
   * @return User found with this email
   */
  public User findByEmail(String email);

  
  /**
   * @return all users
   */
  
  
  public List<User> getAllUsers();
  
  
  /**
   * @param roleName
   * @return Role found with this roleName
   */
  public Role getRoleByName(String roleName);
  
  /**
   * @param userActive
   * 
   */
  public void setUserActive(boolean userActive, User user);
  
  /**
   * @param token
   * @return User found with this token
   */
  public User findByToken(String token);


}
