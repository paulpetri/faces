package com.pmp.exceptions;

public class RequestException extends Exception{
	
	 public static final String BAD_REQUEST = "400";
	 public static final String UNAUTORIZED = "401";
	 public static final String NOT_FOUND = "404";
	 public static final String METHOD_NOT_ALLOWED = "405";
	 
	 public RequestException(String TYPE_ERROR, String message){
		 super(message, new Throwable(TYPE_ERROR));
	 }
	 
	 public RequestException(String message, Throwable cause) {
		 super(message, cause);
	 }
	 
	 
}
