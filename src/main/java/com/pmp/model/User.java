package com.pmp.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(unique = true)
	private String username;

	@Column(unique = true)
	private String email;
	
	private String token;
	
	private String firstName;
	
	private String lastName;
	
	private String password;
	
	private Date tokenGenerated;

	private boolean isActive;
	
	@Column(columnDefinition = "LONGBLOB")
	private byte[] profilePhoto;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "link_userrole", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = { @JoinColumn(name = "roleId") })
	private Set<Role> roles;

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public byte[] getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(byte[] profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Date getTokenGenerated() {
		return tokenGenerated;
	}

	public void setTokenGenerated(Date tokenGenerated) {
		this.tokenGenerated = tokenGenerated;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	/**
	 * Converts the User into a JSONObject and returns it.
	 * 
	 * @return A JSONObject representing an User
	 */
	public JSONObject toJSONObject() {
		JSONObject userJSON = new JSONObject();

		userJSON.put("id", id);
		userJSON.put("username", username);
		userJSON.put("email", email);
		userJSON.put("role", getRolesJson());
		userJSON.put("firstName", firstName);
		userJSON.put("lastName", lastName);
		userJSON.put("isActive", isActive);
		userJSON.put("profilePhoto", profilePhoto);

		return userJSON;
	}

	/**
	 * 
	 * @return {@link JSONArray} of {@link Role} that this user has
	 */
	private JSONArray getRolesJson() {
		JSONArray rolesArray = new JSONArray();
		if(roles!=null && roles.size()>0){
			for (Role role : roles) {
				JSONObject roleJson = role.toJSONObject();
				rolesArray.add(roleJson);
			}
		}
		return rolesArray;
	}

}
