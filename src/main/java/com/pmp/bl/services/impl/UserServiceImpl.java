package com.pmp.bl.services.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import util.AuthProvider;
import util.JWTTokenUtil;
import util.MailUtil;
import util.RoleName;

import com.pmp.bl.services.UserService;
import com.pmp.dao.UserDAO;
import com.pmp.exceptions.RequestException;
import com.pmp.model.Role;
import com.pmp.model.User;
import com.pmp.rest.dto.UserDTO;

/**
 * @author PPaul
 *
 */
public class UserServiceImpl implements UserService {

  /**
   *
   */
  @Autowired
  private UserDAO userDao;

  @Autowired
  private Mapper mapper;

 

  /**
   *  Method called when a new account is created
   */
  @Transactional
  @Override
  public void createAccount(UserDTO userDTO, String requestURL) {
	
	JWTTokenUtil jwtToken = new JWTTokenUtil();
	
	Set<Role> roles = new HashSet<Role>();
	roles.add(userDao.getRoleByName(RoleName.USER));
	userDTO.setRoles(roles);  
    
	AuthProvider auth = new AuthProvider();
    userDTO.setPassword(auth.hashPassword(userDTO.getPassword()));
    
    User user = mapper.map(userDTO, User.class);
    userDTO.setToken(jwtToken.createJWT(user));
    System.out.println("###########");
    System.out.println(userDTO.getToken());
    user.setToken(userDTO.getToken());
    
    Long id = userDao.createAccount(user);
    
    if(id != null){
    	System.out.println("user successfull");
        /* Send mail for confirmation */
        MailUtil mail= new MailUtil();
        mail.sendMail(userDTO.getToken(), userDTO.getEmail(), requestURL);
    	
    } else{
    	System.out.println("fail");
    }
    
    

    
  }
  
  @Override
  @Transactional
  public Map<String, Object> logIn(UserDTO userDTO) throws RequestException {
	JWTTokenUtil jwtToken = new JWTTokenUtil();
    
	User user = userDao.findByUsername(userDTO.getUsername());

    Map<String, Object> logInRespone = new HashMap<String, Object>();

    if (user != null && user.getIsActive()) {

	      AuthProvider auth = new AuthProvider();
	      boolean logInSuccesfull = auth.checkPassword(userDTO.getPassword(), user.getPassword());
	
	      if (logInSuccesfull) {
	    	System.out.println("@@@@@@");
	        userDTO = mapper.map(user, UserDTO.class);
	        userDTO.setToken(jwtToken.createJWT(user));
	        System.out.println(userDTO.getToken());
	        userDTO.setRoles(user.getRoles());
	        System.out.println("#######");
	        logInRespone.put("202", userDTO.toJSONObject());
	
	      } else {
	    	  throw new RequestException(RequestException.UNAUTORIZED, "Password and Username do not match");
	      }

    } else {

    	 throw new RequestException(RequestException.NOT_FOUND, "User not found or is inactive");

    }

    return logInRespone;
  }

  @Override
  @Transactional
  public String getGoogleUser(String gToken) {

    StringBuilder sb = new StringBuilder();
    URLConnection urlConnection = null;
    InputStreamReader in = null;

    try {

      URL gooleUserInfoURL =
          new URL("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + gToken);

      urlConnection = gooleUserInfoURL.openConnection();

      if (urlConnection != null) {
        in = new InputStreamReader(urlConnection.getInputStream(), Charset.defaultCharset());
        BufferedReader bufferedReader = new BufferedReader(in);
        if (bufferedReader != null) {
          int cp;
          while ((cp = bufferedReader.read()) != -1) {
            sb.append((char) cp);
          }
          bufferedReader.close();
        }

      }

    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }


    return sb.toString();
  }

	@Override
	@Transactional
	public List<User> getAllUsers() {
	    List<User> allUsers = userDao.getAllUsers();
	    return allUsers;
	}

	@Override
	@Transactional
	public User getUserByUsername(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	@Transactional
	public User getUserByEmail(String email) {
		return userDao.findByEmail(email);
	}

	@Override
	public void confirmUser(String token) {
		
		User user = userDao.findByToken(token);
		
		System.out.println("findByToken the token :" + token);
		System.out.println("findByToken the User :" + user);
		
		userDao.setUserActive(true, user);
		
	}
  
  
}
