package com.pmp.bl.services;

import java.util.List;
import java.util.Map;

import com.pmp.exceptions.RequestException;
import com.pmp.model.User;
import com.pmp.rest.dto.UserDTO;

/**
 * @author PPaul
 *
 */
public interface UserService {

	
  /**
   * @param user
   */
  public void createAccount(UserDTO user, String requestURL);

  
  /**
   * @param user
   * @return Map<Response Code, User> Response code = 202 if user login is ok = 204 if no user is
   *         found with this username = 401 if the user login fails
   */
  public Map<String, Object> logIn(UserDTO user) throws RequestException;


  /**
   * @param gToken
   * @return
   */
  public String getGoogleUser(String gToken);

  
  /**
   * @param username
   * @return User
   */
  public User getUserByUsername(String username);
 
  
  /**
   * @param email
   * @return User
   */
  public User getUserByEmail(String email);
  
  
  /**
   * @return all users
   */
  public List<User> getAllUsers();
  
  /**
   * @return all users
   */
  public void confirmUser(String token);

}
