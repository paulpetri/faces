package com.pmp.util;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.pmp.model.User;

/**
 * This util class provides methods to create and encrypt JSON Web Tokens and to decrypt them.
 * 
 * @author MariusMa
 */
public class JWTTokenUtil {

  private static final String SHARED_KEY = "a0a2abd8-6162-41c3-83d6-1cf559b46afc";
  private static final Logger log = Logger.getLogger(JWTTokenUtil.class);

  /**
   * Creates a JSON Web Token with a {@link User}'s data in it, serialized it and returns it
   * 
   * @param user The {@link User} that has to be serialized
   * @return A {@link String} representing the token
   */
  public String createJWT(User user) {
    try {
      // Create JWS payload
      Payload payload = new Payload(user.toJSONObject());

      // Create JWS header with HS256 algorithm
      JWSHeader header = new JWSHeader(JWSAlgorithm.HS256);
      header.setContentType("text/plain");

      // Create JWS object
      JWSObject jwsObject = new JWSObject(header, payload);

      JWSSigner signer = new MACSigner(SHARED_KEY.getBytes());
      jwsObject.sign(signer);
      return jwsObject.serialize();
    } catch (JOSEException e) {
      log.error(e.getMessage(), e);
    }

    return null;
  }

  /**
   * Retrieves the JSON representation of an user from the given token, or null if there exists
   * errors
   * 
   * @param token
   * @return JSON Representation of an user
   */
  public JSONObject getUserJSONFromJWT(String token) {
    Payload payload = deserializeJWT(token);
    if (payload != null) {
      return payload.toJSONObject();
    } else {
      return null;
    }
  }

  /**
   * Retrieves the Payload from a given token
   * 
   * @param token
   * @return {@link Payload}
   */
  private Payload deserializeJWT(String token) {
    try {
      // Parsing back and checking signature
      JWSObject jwsObject = JWSObject.parse(token);
      JWSVerifier verifier = new MACVerifier(SHARED_KEY.getBytes());

      if (jwsObject.verify(verifier)) {
        return jwsObject.getPayload();
      }
    } catch (ParseException e) {
      log.error(e.getMessage(), e);
    } catch (JOSEException e) {
      log.error(e.getMessage(), e);
    }

    return null;
  }

  /**
   * Compares the token from the session with the one from the request header and returns true if
   * they are the same or false otherwise
   * 
   * @param req
   * @return <code>true</code> if the tokens are the same, <code>false</code> otherwise
   */
  public static boolean checkToken(HttpServletRequest req) {
    return req.getSession().getAttribute("token") != null
        && req.getSession().getAttribute("token").equals(req.getHeader("authorization"));
  }

  /**
   * This method checks if a user has admin rights
   * 
   * @param req
   * 
   * @return true if user is admin otherwise false
   */
  /*
   * public static boolean checkIsAdmin(HttpServletRequest req) { String token = (String)
   * req.getSession().getAttribute("token");
   * 
   * if (checkToken(req)) { // Check if the tokens are the same (the user is logged in) if (token !=
   * null) { JWTTokenUtil tokenUtil = new JWTTokenUtil(); net.minidev.json.JSONObject userJSON =
   * tokenUtil.getUserJSONFromJWT(token); String username = (String) userJSON.get("username");
   * String role = (String) userJSON.get("role");
   * 
   * // If there are no errors, populate the JSON with data if (username != null && role != null) {
   * if (role.equals(UserRoles.ADMINISTRATOR.toString())) { return true; } } } } return false; }
   */

  /**
   * This method checks if a the role of the client is user or guest
   * 
   * @param req
   * 
   * @return true if user otherwise false
   */
  /*
   * public static boolean checkIsUser(HttpServletRequest req) { String token = (String)
   * req.getSession().getAttribute("token");
   * 
   * if (checkToken(req)) { // Check if the tokens are the same (the user is logged in) if (token !=
   * null) { JWTTokenUtil tokenUtil = new JWTTokenUtil(); net.minidev.json.JSONObject userJSON =
   * tokenUtil.getUserJSONFromJWT(token); String username = (String) userJSON.get("username");
   * String role = (String) userJSON.get("role");
   * 
   * // If there are no errors, populate the JSON with data if (username != null && role != null) {
   * if ((role.equals(UserRoles.ADMINISTRATOR.toString())) ||
   * (role.equals(UserRoles.REGULAR_USER.toString()))) { return true; } } } } return false; }
   */
}
