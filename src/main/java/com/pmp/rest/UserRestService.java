//package com.pmp.rest;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.HeaderParam;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.Context;
//import javax.ws.rs.core.MediaType;
//
//import org.codehaus.jettison.json.JSONException;
//import org.codehaus.jettison.json.JSONObject;
//
//import com.pmp.dao.impl.UserDAO;
//import com.pmp.model.User;
//import com.pmp.rest.dto.UserDTO;
//import com.sun.research.ws.wadl.Request;
//
//
//
///**
// * @author PPaul
// *
// */
//@Path("/users")
//public class UserRestService {
//
//  /**
//   * @param username
//   * @param password
//   * @param req
//   * @return response containing user
//   * @throws JSONException
//   */
//  @POST
//  @Path("/login")
//  @Produces(MediaType.APPLICATION_JSON)
//  public Object login(@HeaderParam("username") String username,
//      @HeaderParam("password") String password, @Context HttpServletRequest req)
//      throws JSONException {
//
//    JSONObject response = new JSONObject();
//    
//    System.out.println("here i am");
//    
//    
//    if (username == null || password == null) {
//    	response.put("error", 0);
//        response.put("login", "Completeaza toate campurile");
//        return response;
//    }
//
//    	System.out.println("-----------------");
//    	System.out.println(username +" -- "+password);
//    	
//    	
//      try {
//        response.put("user", "paul");
//        response.put("error", "0");
//        
//        
//        User user = new User();
//        user.setUsername("paul");
//        user.setEmail("paul@gmail.com");
//        user.setPassword("pass");
//        
//        UserDAO userDao = new UserDAO();
//        userDao.insertUser(user);
//        
//        
//        
//      } catch (Exception e) {
//        e.printStackTrace();
//        response.put("error", 1);
//        response.put("login", "Error");
//        response.put("message",
//            "Ceva nu este inregula. Te rugam sa anunti administratorul de retea.");
//
//      }
//    
//
//    return response;
//  }
//  
//  @POST
//  @Consumes (MediaType.APPLICATION_JSON)
//  @Produces(MediaType.APPLICATION_JSON)
//  public Object createAccount(UserDTO userDto, 
//		  @Context HttpServletRequest req){
//
//    JSONObject response = new JSONObject();
//
//    System.out.println("--- Create Account ---");
//
//    if (username != null && password != null) {
//    	
//		String jsonString;
//		
//		try {
//			 BufferedReader reader = new BufferedReader(new InputStreamReader(requestBody));
//			 StringBuilder out = new StringBuilder();
//		     String line;
//		     
//		     while ((line = reader.readLine()) != null) {
//		            out.append(line);
//		     }
//		     
//		     String image=out.substring(11, out.length());
//		     System.out.println(image);
//		     
//		     byte[] byteConent = image.getBytes();
//		        
//		        User user = new User();
//		        user.setUsername(username);
//		        user.setEmail(username);
//		        user.setPassword(password);
//		        user.setProfilePhoto(byteConent);
//		        
//		        UserDAO userDao = new UserDAO();
//		        userDao.insertUser(user);
//		     
//		     reader.close();
//		
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//   
//        try {
//			response.put("user", "paul");
//			response.put("error", "0");
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//        
//        
//
//        
//        
//        
//     
//
//    } else {
//
//    	 try {
// 			response.put("user", "paul");
// 			response.put("error", "0");
// 		} catch (JSONException e) {
// 			// TODO Auto-generated catch block
// 			e.printStackTrace();
// 		}
//      return response;
//    }
//
//    return response;
//  }
//  
//  @POST
//  @Path("/geUserByUsername")
//  @Produces(MediaType.APPLICATION_JSON)
//  public Object geUserByUsername(@HeaderParam("username") String username,
//      @Context HttpServletRequest req)
//      throws JSONException {
//
//    JSONObject response = new JSONObject();
//
//    System.out.println("--- Find User ---");
//
//    if (username != null ) {
//    	
//    	System.out.println(username );
//    	
//    	
//      try {
//    	  
//    	UserDAO userDao = new UserDAO();  
//      
//    	User user = userDao.findByUsername(username);
//    	
//    	String str = new String(user.getProfilePhoto());
//    	System.out.println(str);
//    	
//    	response.put("username", user.getUsername());
//    	response.put("photo", str);
//        response.put("error", "0");
//        
//      } catch (Exception e) {
//        e.printStackTrace();
//        response.put("error", 1);
//        response.put("getUser", "Error");
//        response.put("message",
//            "No user found with this username");
//
//      }
//
//    } else {
//
//      response.put("error", 0);
//      response.put("getUser", "Please fill all text fields");
//      return response;
//    }
//
//    return response;
//  }
//
//
//}
