package com.pmp.rest.dto;

import java.nio.charset.StandardCharsets;
import java.util.Set;

import com.pmp.model.Role;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

public class UserDTO {

	private String username;

	private String password;

	private String profilePhoto;

	private String email;
	
	private String firstName;
	
	private String lastName;
	
	private String token;

	private Set<Role> roles;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfilePhoto() {
		return profilePhoto;
	}

	public void setProfilePhoto(String profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

	public byte[] getArrayProfilePhoto() {
		return profilePhoto.getBytes(StandardCharsets.UTF_8);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public JSONObject toJSONObject() {
		JSONObject userJSON = new JSONObject();

		userJSON.put("token", token);
		userJSON.put("username", username);
		userJSON.put("profilePhoto", profilePhoto);
		userJSON.put("email", email);
		userJSON.put("firstName", firstName);
		userJSON.put("lastName", lastName);
		userJSON.put("roles", getRolesJson());
	
		return userJSON;
	}
	
	/**
	 * 
	 * @return {@link JSONArray} of {@link Role} that this user has
	 */
	private JSONArray getRolesJson() {
		JSONArray rolesArray = new JSONArray();
		for (Role role : roles) {
			JSONObject roleJson = role.toJSONObject();
			rolesArray.add(roleJson);
		}
		return rolesArray;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
