package com.pmp.rest.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.apache.cxf.jaxrs.ext.MessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pmp.bl.services.UserService;
import com.pmp.exceptions.RequestException;
import com.pmp.model.User;
import com.pmp.rest.dto.UserDTO;
import com.pmp.rest.service.UserRestService;
import util.ResponseHandle;

/**
 * @author PPaul
 *
 */
@Service("userRestService")
public class UserRestServiceImpl implements UserRestService {

	@Context
	private MessageContext context;

	@Autowired
	private UserService userService;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Response logIn(UserDTO user) {

		Map<String, Object> loginResponse = null;
		
		try{
			loginResponse = userService.logIn(user);
			return Response.ok(("{\"user\":" + loginResponse.get("202")) + "}").build();
		}catch(RequestException e){
			
			return new ResponseHandle().buildResponse(e);
		}

	}

	@Override
	public Response signIn(UserDTO user) {

		HttpServletRequest req = context.getHttpServletRequest();
		userService.createAccount(user, req.getRequestURL().toString());

		return Response.ok("I am in ").build();
	}

	@Override
	public Response googlelogin(JSONObject json) {

		String gToken = (String) json.get("gToken");
		return Response.ok(userService.getGoogleUser(gToken)).build();

	}

	@Override
	public Response authorize(UserDTO user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response getAllUsers() {

		List<User> allUsers = userService.getAllUsers();

		JSONArray jsonArray = new JSONArray();

		for (User user : allUsers) {
			jsonArray.add(user.toJSONObject());
		}

		return Response.ok("{\"users\":" + jsonArray + "}").build();
	}

	@Override
	public Response searchUser(String username, String email) {
		User foundUser;
		if (username != null) {
			foundUser = userService.getUserByUsername(username);
			if (foundUser != null) {
				return Response.ok(
						"{\"username\":\"" + foundUser.getUsername() + "\"}")
						.build();
			} else {
				return Response.ok("{\"username\":\"" + null + "\"}").build();
			}
		}

		if (email != null) {
			foundUser = userService.getUserByEmail(email);
			if (foundUser != null) {
				return Response.ok(
						"{\"username\":\"" + foundUser.getEmail() + "\"}")
						.build();
			} else {
				return Response.ok("{\"username\":\"" + null + "\"}").build();
			}
		}

		return null;
	}

	@Override
	public void confirmUser(String token) {
		System.out.println("#### in confirm user " + token);
		userService.confirmUser(token);
	}

}
