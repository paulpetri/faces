package com.pmp.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.WebServiceContext;

import net.minidev.json.JSONObject;

import com.pmp.exceptions.RequestException;
import com.pmp.rest.dto.UserDTO;


/**
 * @author PPaul
 *
 */
@Path("/user")
@Produces("application/json")
public interface UserRestService {

  /**
   * @param username
   * @param password
   * @return response
   */
  @POST
  @Path("/login")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response logIn(UserDTO user);

  @POST
  @Path("/authorize")
  public Response authorize(UserDTO user);

  @POST
  @Path("/google")
  public Response googlelogin(JSONObject token);

  /**
   * @return response
   */
  @POST
  @Path("/signin")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response signIn(UserDTO user);
  
  @GET
  @Path("/signin/confirm")
  public void confirmUser(@QueryParam("token") String token);

  @GET
  @Path("/all")
  public Response getAllUsers();
  
  @GET
  @Path("/search")
  public Response searchUser(@HeaderParam("username") String username, @HeaderParam("email") String email);
}
